<?php
namespace App\Helpers;
use Request;
class Helper{
	/**
     * Check for required fields
     */
    public static function check_required() {
        $data = Request::all();

        foreach (func_get_args() as $arg) {
            if (!Request::has($arg)) {
                return false;
            }
        }

        return true;
    }
}
?>