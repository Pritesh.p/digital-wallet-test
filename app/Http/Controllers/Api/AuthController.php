<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Hash;
use Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        
         $user=User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'api_token' => Str::random(80),
        ]);

        return [
            'status' => 200,
            'msg' => "Registration Successfully",
            'api_token' => $user->api_token
        ];
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        if (! $token = auth()->attempt($credentials)) {
            return [
                'status' => 500,
                'msg' => "Login Failed"
            ];
        }

      $token = Str::random(80);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();
        return [
            'status' => 200,
            'msg' => "Login Successfully",
            'api_token' => $token
        ];
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    
}