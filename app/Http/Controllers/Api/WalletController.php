<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Wallet;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Helpers\Helper;
// use Illuminate\Support\Facades\Input;

class WalletController extends Controller
{

    public function addMoney(Request $request)
    {
     if (!Helper::check_required('user_id','amount','card_number','card_pin','api_token')) {
            return [
                'status' => 500,
                'msg' => "Fill all the details"
            ];
        }  

        $wallet = Wallet::firstOrNew(array('user_id' => $request->user_id));
        $wallet->amount += $request->amount;
        $wallet->save();

        return [
            'status' => 200,
            'msg' => "Money Addedd Successfully"
        ];            


        dd($addMoney);
    }

    
}